from .tools import morseQuery
from warnings import warn


def is_ascii(s: chr) -> bool:
    isAscii = True
    try:
        s = s.encode()
        s.decode('ascii')
    except UnicodeDecodeError:
        isAscii = False
    return isAscii


class morse:

    # The parser for the morse translations.
    parser = None

    def __init__(self):
        self.parser = morseQuery()

    def fromMorse(self, morseSource: str) -> str:
        """
        ``morseSource``: should hold the morse code, which will be transformed to ascii characters. (str)\n
        Will change the ``morseSource`` parameter to clear ascii text, and returns it.
        """
        clearString = ""
        # Split the string via dots, and iterate over the array.
        for i in morseSource.split("."):
            # Find corresponding ascii character
            morseChar = self.parser[i]
            # Check if there's two consecutive dots, and that the morse
            # has a corresponding ascii character
            if i != "" and morseChar != -1:
                clearString += morseChar
            else:
                # If there was a failure with the transformation, just append an error
                # note to the output, and display a warning.
                clearString += "(({})?)".format(i)
                warn(
                    ("Two consecutive dots or corrupt morse code caused"
                     " a warning, replacing with (({})?) and skipping".format(i)),
                    stacklevel=5
                )

        return clearString

    def toMorse(self, strSource: str) -> str:
        """
        ``strSource``: should hold the ascii text, which will be transformed to morse code. (str)\n
        Will change the ``strSource`` parameter to morse code string, and returns it.
        """
        morseString = ""
        # Iterate over all characters in the string.
        for i in strSource:
            # Find corresponding morse code to the ascii character
            morseCode = self.parser[i]

            # Check if the character is ascii and that there was a corresponding morse code.
            if morseCode != -1 and is_ascii(i):
                morseString += morseCode + "."
            else:
                # If there was a failure with the transformation, just append an error
                # note to the output, and display a warning.
                morseString += "(({})?)".format(i) + "."
                warn(
                    ("Character '{}' doesn't have a corresponding morse code,"
                     " replacing with '(({})?)' and ignoring.".format(i, i)),
                    stacklevel=5
                )
        return morseString[:-1]
