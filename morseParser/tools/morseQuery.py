from codecs import open as utfOpen
from inspect import stack
from json import JSONDecodeError, dumps, loads
from warnings import warn


class morseQuery:

    # the queryable dictionaries.
    __morseDict = {}
    __charDict = {}

    # important folder and file paths
    __workingDir = stack()[0][1][:-13]
    __currJsonPath = __workingDir + "morseCodes.json"
    __currParsedJsonPath = __workingDir + "parsedMorse.json"

    # objects runtime exceptions
    morseParseException = ("Format within '{}' JSON file is either corrupted"
                           " or formatted incorrectly.")
    morseSourceException = "Given JSON file doesn't exist, check '{}' location."

    def __init__(self, conf: str=None) -> None:
        """
        ``conf``: Defines a custom path to morse code key:value pair file.

        Creates the default runtime exceptions, as well as populates the 
        ``__morseDict`` and ``__charDict`` dictionaries from the morse 
        code key:value pair file.
        """
        self.morseParseException = self.morseParseException.format(
            self.__currJsonPath)
        self.morseSourceException = self.morseSourceException.format(
            self.__currJsonPath)

        if conf is not None:
            self.__currJsonPath = conf
            self.__morseSerializer(self.__morseFileHandler())
        else:
            self.__parsedLoader()

    def __parsedLoader(self) -> None:
        """
        Will load the 'parsedMorse.json' file, instead of 'morseCodes.json'. 
        Will optimize the initialization of the object, since there's no 
        need to create the recursively queryable dictionary for the 
        morse -> char queries.
        """
        try:
            jsonFile = utfOpen(self.__currParsedJsonPath, "r", "utf-8")
            tempDict = loads(jsonFile.read())
            jsonFile.close()
            self.__morseDict = tempDict["toMorse"]
            self.__charDict = tempDict["toChar"]
        except:
            # No specific exceptions will be caught, due to there being multiple possibilities.
            # Most of the exceptions will lead to this option.
            warn(
                ("'parsedMorse.json' file is in someway corrupted, will load the"
                 " default morse codes andrecreate the json file based upon those."),
                stacklevel=5
            )
            self.__morseSerializer(self.__morseFileHandler())
            self.newParsedMorse()

    def __morseFileHandler(self) -> str:
        """
        Reads the given or default key-value pair json file, and returns the contents. 
        Throws a custom ``Exception``, if the provided json file is not located or on decode error. 
        """

        newFile, jsonString = None, None
        try:
            newFile = utfOpen(self.__currJsonPath, "r", "utf-8")
            jsonString = newFile.read()
        except:
            raise Exception(self.morseSourceException)

        newFile.close()

        return jsonString

    def __morseSerializer(self, morseString: str) -> None:
        """
        ``morseString``: The string holding within itself the json format for the key-value pairs 
        of char-morse conversions (str)\n

        Parses through the given json source, and provides the queryable dictionaries for the object.
        """
        trueMorseArray = None
        # Load the json source to a dictionary, and retrieve the (char,morse code) array.
        try:
            jsonDict = loads(morseString)
            trueMorseArray = jsonDict["morseCodes"]
        except JSONDecodeError:
            raise Exception(self.morseParseException)
        # Individually goes through all (char,morse code) pairs, and place them
        # within queryable dictionaries.
        i = 0
        while i < len(trueMorseArray):
            try:
                self.__morseDict[trueMorseArray[i][0]] = trueMorseArray[i][1]
                self.__recursiveOrganizer(
                    trueMorseArray[i][0], trueMorseArray[i][1], self.__charDict)
            except IndexError:
                # If a (char, morse code) pair is somehow corrupted, raise a warning, and continue on.
                warn(
                    self.morseParseException +
                    "\nIgnoring {}. index of 'morseCodes' array.".format(i),
                    stacklevel=5
                )
            i += 1

    def __recursiveOrganizer(self, key: chr, value: str, curr: dict) -> None:
        """
        ``key``: The character which will be placed at the end of the path (chr)\n
        ``value``: The string, upon which the path in the dictionary will be created (str)\n
        ``curr``: The dictionary, which will receive a structure of ``value``, and value of ``key`` (dict)\n
        Recursively creates new key-dictionary pairs within the ``curr`` dictionary, based upon the ``value``
        array of characters, in order to create a recursively queryable path. Places the ``key`` parameter at 
        the end of the created path.
        """
        if len(value):
            if value[0] not in curr:
                curr[value[0]] = {}
            self.__recursiveOrganizer(key, value[1:], curr[value[0]])
        else:
            curr["chr"] = key

    def __recursiveGetter(self, key: str, curr: dict) -> chr:
        """
        ``curr``: The queried dictionary, which should hold within itself character matches.\n
        ``key``: The morse code, upon which the ``curr`` dictionary will be queried.\n
        Will go recursively through the '__charDict' dictionary, and will return the character which matches
        the morse code.\n
        Throws 'KeyError' if a key-value pair couldn't be formed.
        """
        if len(key):
            return self.__recursiveGetter(key[1:], curr[key[0]])
        else:
            return curr['chr']

    def __getitem__(self, index: str) -> str:
        """
        ``index``: either morse code or a character, which will be converted to vice-versa.
        Override of [] operators for the 'morseCodes' object.

        """

        res = None
        try:
            firstChar = index[0]
            if firstChar == '•' or firstChar == '−':
                res = self.__recursiveGetter(index, self.__charDict)
            else:
                res = self.__morseDict[index.upper()]
        except:
            # Mostly relevant for __recursiveGetter function, but for clarity, placed for both key queries.
            res = -1
        return res

    def newParsedMorse(self) -> None:
        """
        If updates are applied to default 'morseCodes.json' file, you should run this method
        in order to update the parsed 'parsedMorse.json' file. 
        """
        writeFile = utfOpen(self.__currParsedJsonPath, "w", "utf-8")
        writeFile.write(dumps(
            {"toChar": self.__charDict, "toMorse": self.__morseDict}, indent=4, sort_keys=True))
        writeFile.close()


if __name__ == "__main__":
    loadObject = morseQuery()
    loadObject.newParsedMorse()
