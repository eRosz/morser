from codecs import open as cOpen

# Just a mockup of a custom IO


class fileReq:

    source = None

    def __init__(self, request: str, init: bool=True) -> None:
        """
        ``request``: The request string, which will be displayed to the user (str)\n
        ``init`` : Whether the file path should be queried and read. (bool)\n
        Asks the user for a path to a file, and either holds the path, or reads it.

        """
        print(request + ":")
        self.path = input("-> ")
        if init:
            self.recvFile()

    def writeFile(self, source: str, codec: str="utf-8"):
        """
        ``source``: The source which will be written to the file (str)\n
        ``codec`` : The encoding used to write to the file. (str)\n
        Writes the ``source`` parameter to the file, which is located in the ``self.path`` location.

        """
        writeFile = None
        try:
            writeFile = cOpen(self.path, "w", codec)
            writeFile.write(source)
        except:
            raise Exception(("Either the file path is not valid, or the"
                             " writable has non-comforming characters."))
        writeFile.close()

    def recvFile(self, codec: str="utf-8-sig") -> str:
        """
        ``codec`` : The encoding used to write to the file. (str)\n
        Reads the file which is located in the ``self.path`` location, and stores it in the object.

        """
        contents = None
        try:
            sourceFile = cOpen(self.path, "r", codec)
            contents = sourceFile.read()
            sourceFile.close()
        except:
            raise IOError
        self.source = contents

    def __str__(self):
        """
        Returns the source, which was read from the file.
        Returns an empty string if no file has been read.
        """
        return self.source if self.source is not None else ""


# Example of how it should be used.
if __name__ == "__main__":
    print(fileReq("Give the path"))
