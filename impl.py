from morseParser import morse
from morseParser.tools import fileReq

# Merely an example of implementation

source = str(fileReq("Input file"))
output = fileReq("Output file", False)

morseConversion = morse()

userChoice = input(("Do you want to:\n   (1) transform from morse to string\n"
                    "   (2) transform from string to morse\n: "))

transformed = ""

if userChoice == "1":
    transformed = morseConversion.fromMorse(source)
elif userChoice == "2":
    transformed = morseConversion.toMorse(source)
else:
    print("Command not recognized, only 1 or 2 possible. ")

output.writeFile(transformed)
